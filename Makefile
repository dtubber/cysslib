.PHONY: clean All

All:
	@echo "----------Building project:[ cysslib - Debug_win64 ]----------"
	@cd "cysslib" && "$(MAKE)" -f  "cysslib.mk"
clean:
	@echo "----------Cleaning project:[ cysslib - Debug_win64 ]----------"
	@cd "cysslib" && "$(MAKE)" -f  "cysslib.mk" clean
