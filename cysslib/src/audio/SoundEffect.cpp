#include <cyss/audio/SoundEffect.hpp>

#include <SFML/Audio.hpp>

using namespace cyss;

SoundEffect::SoundEffect(std::string _filename)
{
    sf::SoundBuffer* sb = new sf::SoundBuffer();
    sb->loadFromFile(_filename);
    sf::Sound* s = new sf::Sound();
    s->setBuffer(*sb);
    m_BufferHandle = (void*)sb;
    m_SoundHandle = (void*)s;
}

SoundEffect::~SoundEffect()
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    delete s;
}

void SoundEffect::play()
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->play();
}

void SoundEffect::pause()
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->pause();
}

void SoundEffect::stop()
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->stop();
}

void SoundEffect::setShouldLoop(bool _shouldLoop)
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->setLoop(_shouldLoop);
}

void SoundEffect::setPitch(float _pitch)
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->setPitch(_pitch);
}

void SoundEffect::setVolume(float _volume)
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->setVolume(_volume);
}

void SoundEffect::setOffsetS(float _offsetS)
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->setPlayingOffset(sf::seconds(_offsetS));
}

void SoundEffect::setOffsetMS(int _offsetMS)
{
    sf::Sound* s = (sf::Sound*)m_SoundHandle;
    s->setPlayingOffset(sf::milliseconds(_offsetMS));
}
