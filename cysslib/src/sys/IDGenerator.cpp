#include <cyss/sys/IDGenerator.hpp>

using namespace cyss;

IDGenerator::IDGenerator()
{
    m_IDSequence = 0;
}

IDGenerator::~IDGenerator()
{
    
}

size_t IDGenerator::getNextID()
{
    size_t ret = m_IDSequence;
    m_IDSequence += 1;
    return ret;
}

size_t IDGenerator::getID(std::string _param)
{
    return m_StringHasher(_param);
}