#include <cyss/sys/GameTime.hpp>

using namespace cyss;

GameTime::GameTime()
{
    m_Last = std::chrono::high_resolution_clock::now();
}

GameTime::~GameTime()
{
    
}

void GameTime::update()
{
    m_Last = std::chrono::high_resolution_clock::now();
}

int GameTime::getElapsedMS()
{
    auto now = std::chrono::high_resolution_clock::now();
    int ret = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_Last).count();
    return ret;
}

int GameTime::getElapsedNS()
{
    auto now = std::chrono::high_resolution_clock::now();
    int ret = std::chrono::duration_cast<std::chrono::nanoseconds>(now - m_Last).count();
    return ret;
}

int GameTime::getElapsedS()
{
    auto now = std::chrono::high_resolution_clock::now();
    int ret = std::chrono::duration_cast<std::chrono::seconds>(now - m_Last).count();
    return ret;
}
