#include <cyss/sys/Kernel.hpp>

#include <utility>
#include <chrono>
#include <sstream>

#include <cyss/util/Logger.hpp>

using namespace cyss;

Kernel::Kernel()
{
    m_IsRunning = false;
    m_WorkerCount = 0;
    m_KernelState = KernelState::Shutdown;
}
Kernel::~Kernel()
{
    
}

void Kernel::boot(unsigned int _workerCount)
{
    m_KernelStateMutex.lock();
    m_KernelState = KernelState::Running;
    m_KernelStateMutex.unlock();
    for(unsigned int i = 0; i < _workerCount; i++)
    {
        std::thread t(Kernel::workTask, this, i);
        m_WorkersMutex.lock();
        m_Workers.push_back(std::move(t));
        m_WorkersMutex.unlock();
    }
}

void Kernel::run()
{
    m_IsRunningMutex.lock();
    m_IsRunning = true;
    m_IsRunningMutex.unlock();
    m_LoopTasksMutex.lock();
    for(unsigned int i = 0; i < m_LoopTasks.size(); i++)
    {
        std::shared_ptr<Task> task = m_LoopTasks.front();
        m_LoopTasks.pop();
        std::thread t([task,this](){
            bool is_running = true;
            while (is_running)
            {
                m_IsRunningMutex.lock();
                is_running = m_IsRunning;
                m_IsRunningMutex.unlock();
                task->_execute();
            }
        });
        m_LoopWorkersMutex.lock();
        m_LoopWorkers.push_back(std::move(t));
        m_LoopWorkersMutex.unlock();
    }
    m_LoopTasksMutex.unlock();
    while(true)
    {
        std::unique_lock<std::mutex> isrunningLocker(m_IsRunningMutex);
        m_IsRunningCond.wait(isrunningLocker, [this](){ return !m_IsRunning; });
        if(!m_IsRunning)
        {
            isrunningLocker.unlock();
            m_KernelStateMutex.lock();
            m_KernelState = KernelState::Shutdown;
            m_KernelStateMutex.unlock();
            m_KernelStateCond.notify_all();
            break;
        }
        else
        {
            isrunningLocker.unlock();
        }
    }
    m_LoopWorkersMutex.lock();
    for(std::thread& t : m_LoopWorkers)
    {
        t.join();
    }
    m_LoopWorkers.clear();
    std::stringstream ss1;
    ss1 << "Number of LoopWorkers still alive: " << m_LoopWorkers.size();     Logger::getInstancePtr()->consoleLog("KERNEL", ss1.str());
    m_LoopWorkersMutex.unlock();
    m_WorkersMutex.lock();
    for(std::thread& t : m_Workers)
    {
        t.join();
    }
    m_Workers.clear();
    std::stringstream ss2;
    ss2 << "Number of Workers still alive: " << m_Workers.size();
    Logger::getInstancePtr()->consoleLog("KERNEL", ss2.str());
    m_WorkersMutex.unlock();
    m_TasksMutex.lock();
    while(!m_Tasks.empty())
    {
        m_Tasks.pop();
    }
    std::stringstream ss3;
    ss3 << "Number of Tasks still left in queue: " << m_Tasks.size();
    Logger::getInstancePtr()->consoleLog("KERNEL", ss3.str());
    m_TasksMutex.unlock();
    m_LoopTasksMutex.lock();
    while(!m_LoopTasks.empty())
    {
        m_Tasks.pop();
    }
    std::stringstream ss4;
    ss4 << "Number of LoopTasks still left in queue: " << m_LoopTasks.size();
    Logger::getInstancePtr()->consoleLog("KERNEL", ss4.str());
    m_LoopTasksMutex.unlock();
}

void Kernel::shutdown()
{
    m_KernelStateMutex.lock();
    KernelState ks = m_KernelState;
    m_KernelStateMutex.unlock();
    if(ks == KernelState::Running || ks == KernelState::WorkAvailable)
    {
        m_IsRunningMutex.lock();
        m_IsRunning = false;
        m_IsRunningMutex.unlock();
        m_IsRunningCond.notify_one();
    }
}

void Kernel::workTask(unsigned int _workerID)
{
    std::stringstream ss;
    ss << "Worker #" << _workerID;
    Logger::getInstancePtr()->consoleLog(ss.str(), "starting.");
    bool is_running = true;
    while(true)
    {
        m_IsRunningMutex.lock();
        is_running = m_IsRunning;
        m_IsRunningMutex.unlock();
        if(!is_running) break;
        std::unique_lock<std::mutex> kernelstateLocker(m_KernelStateMutex);
        m_KernelStateCond.wait(kernelstateLocker);
        if(m_KernelState == KernelState::Shutdown)
        {
            kernelstateLocker.unlock();
            Logger::getInstancePtr()->consoleLog(ss.str(), "received break signal!");
            break;
        }
        else if(m_KernelState == KernelState::Running) continue;
        else if(m_KernelState == KernelState::WorkAvailable)
        {
            m_TasksMutex.lock();
            std::shared_ptr<Task> task = m_Tasks.front();
            m_Tasks.pop();
            if(m_Tasks.empty()) m_KernelState = KernelState::Running;
            kernelstateLocker.unlock();
            m_TasksMutex.unlock();
            Logger::getInstancePtr()->consoleLog(ss.str(), "got work! executing task!");
            task->_execute();
            task->setFinished();
        }
    }
    Logger::getInstancePtr()->consoleLog(ss.str(), "terminating.");
}
void Kernel::addLoopTask(std::shared_ptr<LoopTask> _task)
{
    m_IsRunningMutex.lock();
    bool is_running = m_IsRunning;
    m_IsRunningMutex.unlock();
    if(is_running)
    {
        m_LoopWorkersMutex.lock();
        std::thread t([_task,this](){
            bool is_running = true;
            float current_max_frequency = 0.0f;
            float current_min_frametime = 0.0f;
            while(is_running)
            {
                m_IsRunningMutex.lock();
                is_running = m_IsRunning;
                m_IsRunningMutex.unlock();
                current_max_frequency = _task->getMaxFrequency();
                current_min_frametime = 1.0f/current_max_frequency;
                unsigned int frame_milliseconds = (unsigned int)current_min_frametime;
                auto begin = std::chrono::high_resolution_clock::now();
                auto end = std::chrono::high_resolution_clock::now();
                unsigned int current_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
                if(current_milliseconds < frame_milliseconds)
                {
                    std::this_thread::sleep_for(std::chrono::milliseconds(frame_milliseconds - current_milliseconds));
                } 
            }
        });
        m_LoopWorkersMutex.unlock();
    }
    else
    {
        m_LoopTasksMutex.lock();
        m_LoopTasks.push(_task);
        m_LoopTasksMutex.unlock();
    }
}
void Kernel::addTask(std::shared_ptr<Task> _task)
{
    m_KernelStateMutex.lock();
    KernelState ks = m_KernelState;
    m_KernelStateMutex.unlock();
    if(ks == KernelState::Running || ks == KernelState::WorkAvailable)
    {
        m_TasksMutex.lock();
        m_Tasks.push(_task);
        m_TasksMutex.unlock();
        m_KernelStateMutex.lock();
        m_KernelState = KernelState::WorkAvailable;
        m_KernelStateCond.notify_one();
        m_KernelStateMutex.unlock();
    }
    
}