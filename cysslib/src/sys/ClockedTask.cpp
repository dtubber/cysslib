#include <cyss/sys/ClockedKernelTask.hpp>


using namespace cyss;

ClockedKernelTask::~ClockedKernelTask()
{
    
}

void ClockedKernelTask::setFrequency(unsigned int _fps)
{
    m_Frequency = _fps;
    m_FrequencySet = true;
    m_FrequencyUnlocked = false;
}

void ClockedKernelTask::setMaxFrequency(unsigned int _fps)
{
    m_MaxFrequency = _fps;
    m_FrequencyUnlocked = true;
    m_FrequencySet = false;
}

void ClockedKernelTask::_execute()
{
    execute();
}

void ClockedKernelTask::execute()
{
    m_Function();
}


