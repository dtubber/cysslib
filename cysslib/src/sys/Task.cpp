#include <cyss/sys/Task.hpp>

using namespace cyss;

Task::Task()
{
    m_HasFunction = false;
}
Task::Task(std::function<void()> _function)
{
    m_HasFunction = true;
    m_Function = _function;
}
Task::~Task()
{
    
}

bool Task::getFinished()
{
    m_Mutex.lock();
    bool finished = m_IsFinished;
    m_Mutex.unlock();
    return finished;
}
void Task::setFinished()
{
    m_Mutex.lock();
    m_IsFinished = true;
    m_Mutex.unlock();
}

void Task::execute()
{
    m_Mutex.lock();
    _execute();
    m_Mutex.unlock();
}

void Task::_execute()
{
    if(m_HasFunction) m_Function();
}

LoopTask::LoopTask(std::function<void()> _function) : Task(_function)
{
    
}

LoopTask::~LoopTask()
{
    
}

void LoopTask::setMaxFrequency(float _frequency)
{
    m_MaxFrequency = _frequency;
}

float LoopTask::getMaxFrequency()
{
    return m_MaxFrequency;
}
