#include <cyss/util/InputManager.hpp>

#include <cyss/util/InputMap.hpp>

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

#include <unordered_map>

using namespace cyss;

InputManager::InputManager(GameWindow* _window)
{
    m_Window = _window;
    GLFWwindow* window = (GLFWwindow*)m_Window->getHandle();
    glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);
}

InputManager::~InputManager()
{
    for(auto pair : m_InputMaps)
    {
        delete pair.second;
    }
}

void InputManager::update()
{
    GLFWwindow* window = (GLFWwindow*)m_Window->getHandle();
    for(auto pair : m_InputMaps)
    {
        double X = 0.0; double Y = 0.0;
        glfwGetCursorPos(window, &X, &Y);
        glm::vec2 mouse_pos((float)X, (float)Y);
        pair.second->m_MousePosition = mouse_pos;
        std::unordered_map<int, int>& mousekeys = pair.second->m_MouseButtonMap;
        for(auto keys : mousekeys)
        {
            int state = glfwGetMouseButton(window, keys.first);
            if(state == GLFW_PRESS)
            {
                pair.second->m_ButtonDownMap[keys.second] = true;
            }
            else
            {
                pair.second->m_ButtonDownMap[keys.second] = false;
            }
        }
        std::unordered_map<int, int>& keyboardkeys = pair.second->m_KeyboardButtonMap;
        for(auto keys : keyboardkeys)
        {
            int state = glfwGetKey(window, keys.first);
            if(state == GLFW_PRESS)
            {
                pair.second->m_ButtonDownMap[keys.second] = true;
            }
            else
            {
                pair.second->m_ButtonDownMap[keys.second] = false;
            }
        }
        std::unordered_map<int, std::unordered_map<int, int>>& joystickkeys = pair.second->m_JoystickButtonMap;
        for(auto jm : joystickkeys)
        {
            int keycount = 0;
            const unsigned char* buttonstates = glfwGetJoystickButtons(jm.first, &keycount);
            for(int i = 0; i < keycount; i++)
            {
                if(jm.second.count(i) != 0)
                {
                    if(buttonstates[i] == GLFW_PRESS)
                    {
                        pair.second->m_ButtonDownMap[jm.second[i]] = true;
                    }
                    else
                    {
                        pair.second->m_ButtonDownMap[jm.second[i]] = false;
                    }
                }
            }
        }
        
        std::unordered_map<int, std::unordered_map<int, int>>& joystickaxes = pair.second->m_JoystickAxisMap;
        for(auto jm : joystickaxes)
        {
            int axiscount = 0;
            const float* axisstates = glfwGetJoystickAxes(jm.first, &axiscount);
            for(int i = 0; i < axiscount; i++)
            {
                if(jm.second.count(i) != 0)
                {
                    pair.second->m_AxisMap[i] = axisstates[i];
                }
            }
        }
    }
}

InputMap* InputManager::createMap(std::string _name)
{
    InputMap* map = new InputMap(this);
    m_InputMaps[_name] = map;
    return map;
}

InputMap* InputManager::getMap(std::string _name)
{
    return m_InputMaps[_name];
}

void InputManager::destroyMap(std::string _name)
{
    delete m_InputMaps[_name];
    m_InputMaps.erase(_name);
}

std::string InputManager::getClipboardString()
{
    std::string s(glfwGetClipboardString((GLFWwindow*)m_Window->getHandle()));
    return s;
}

void InputManager::setClipboardString(std::string _text)
{
    glfwSetClipboardString((GLFWwindow*)m_Window->getHandle(), _text.c_str());
}

int InputManager::getJoystickAxisNumber(Joystick _joystick)
{
    int count = 0;
    glfwGetJoystickAxes(_joystick, &count);
    return count;
}

int InputManager::getJoystickButtonNumber(Joystick _joystick)
{
    int count = 0;
    glfwGetJoystickButtons(_joystick, &count);
    return count;
}

bool InputManager::getJoystickConnected(Joystick _joystick)
{
    if(glfwJoystickPresent(_joystick) == GL_TRUE)
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::string InputManager::getJoystickName(Joystick _joystick)
{
    std::string s(glfwGetJoystickName(_joystick));
    return s;
}
