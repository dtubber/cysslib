#include <cyss/physics/vec4f.hpp>

#include <cmath>

using namespace cyss;

vec4f::vec4f()
{
    X = 0;
    Y = 0;
    Z = 0;
    W = 0;
}

vec4f::vec4f(float _x, float _y, float _z, float _w)
{
    X = _x; 
    Y = _y;
    Z = _z;
    W = _w;
}

vec4f::~vec4f()
{
    
}

float vec4f::getLength()
{
    float ret = (float)sqrt(pow(X, 2) + pow(Y, 2) + pow(Z, 2));
    return ret;
}

vec4f vec4f::operator+(const vec4f& _right)
{
    vec4f ret;
    vec4f& _left = *this;
    ret.X = _left.X + _right.X;
    ret.Y = _left.Y + _right.Y;
    ret.Z = _left.Z + _right.Z;
    ret.W = _left.W + _right.W;
    return ret;
}

vec4f vec4f::operator-(const vec4f& _right)
{
    vec4f ret;
    vec4f& _left = *this;
    ret.X = _left.X - _right.X;
    ret.Y = _left.Y - _right.Y;
    ret.Z = _left.Z - _right.Z;
    ret.W = _left.W - _right.W;
    return ret;
}

vec4f vec4f::operator*(const float _scalar)
{
    vec4f ret;
    vec4f& _left = *this;
    ret.X = _left.X * _scalar;
    ret.Y = _left.Y * _scalar;
    ret.Z = _left.Z * _scalar;
    ret.W = _left.W * _scalar;
    return ret;
}

vec4f& vec4f::operator+=(const vec4f& _other)
{
    X += _other.X;
    Y += _other.Y;
    Z += _other.Z;
    W += _other.W;
    return (*this);
}
vec4f& vec4f::operator-=(const vec4f& _other)
{
    X -= _other.X;
    Y -= _other.Y;
    Z -= _other.Z;
    W -= _other.W;
    return (*this);
}
vec4f& vec4f::operator*=(const float _scalar)
{
    X *= _scalar;
    Y *= _scalar;
    Z *= _scalar;
    W *= _scalar;
    return (*this);
}

vec4f& vec4f::operator=(const vec4f& _other)
{
    
}


vec4f vec4f::cross(const vec4f& _left, const vec4f& _right)
{
    vec4f ret;
    return ret;
}

float vec4f::dot(const vec4f& _left, const vec4f& _right)
{
    float ret;
    ret = (_left.X * _right.X) + (_left.Y * _right.Y) + (_left.Z * _right.Z);
    return ret;
}
