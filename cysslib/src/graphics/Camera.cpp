#include <cyss/graphics/Camera.hpp>

#include <glm/gtc/matrix_transform.hpp>

using namespace cyss;


Camera::Camera()
{
    
}

Camera::~Camera()
{
    
}

glm::mat4 Camera::getProjMatrix()
{
    return m_ProjMatrix;
}

glm::mat4 Camera::getViewMatrix()
{
    glm::vec3 lookat(m_LookAt);
    glm::vec3 center(m_Position);
    glm::vec3 up(m_UpVector);
    
    glm::mat4 view_mat = glm::lookAt(lookat, center, up);
    return view_mat;
}

void Camera::moveBy(glm::vec4 _delta)
{
    m_Position += _delta;
}

void Camera::setPosition(glm::vec4 _position)
{
    m_Position = _position;
}

void Camera::lookAt(glm::vec4 _lookAt)
{
    m_LookAt = _lookAt;
}

void Camera::setUpVector(glm::vec4 _upVector)
{
    m_UpVector = _upVector;
}


