#include <cyss/graphics/VertexBuffer.hpp>

#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>

using namespace cyss;

VertexBuffer::VertexBuffer(int _bytesize)
{
    glGenBuffers(1, &m_ID);
    GLuint binding;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&binding);
    glBindBuffer(GL_ARRAY_BUFFER, m_ID);
    glBufferData(GL_ARRAY_BUFFER, _bytesize, NULL, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, binding);
}

VertexBuffer::~VertexBuffer()
{
    glDeleteBuffers(1, &m_ID);
}

unsigned int VertexBuffer::getID()
{
    return m_ID;
}

void VertexBuffer::upload(std::vector<glm::vec4>& _vertices)
{
    GLuint binding;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&binding);
    glBindBuffer(GL_ARRAY_BUFFER, m_ID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(_vertices), glm::value_ptr(_vertices[0]));
    glBindBuffer(GL_ARRAY_BUFFER, binding);
}

void VertexBuffer::upload(std::vector<glm::mat4>& _matrices)
{
    GLuint binding;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&binding);
    glBindBuffer(GL_ARRAY_BUFFER, m_ID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(_matrices), glm::value_ptr(_matrices[0]));
    glBindBuffer(GL_ARRAY_BUFFER, binding);
}

void VertexBuffer::upload(std::vector<float>& _data)
{
    GLuint binding;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&binding);
    glBindBuffer(GL_ARRAY_BUFFER, m_ID);ss
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(_data), &_data[0]);
    glBindBuffer(GL_ARRAY_BUFFER, binding);
}

void VertexBuffer::upload(std::vector<char>& _data)
{
    GLuint binding;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, (GLint*)&binding);
    glBindBuffer(GL_ARRAY_BUFFER, m_ID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(_data), &_data[0]);
    glBindBuffer(GL_ARRAY_BUFFER, binding);
}
