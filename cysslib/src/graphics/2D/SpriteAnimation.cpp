#include <cyss/graphics/2D/SpriteAnimation.hpp>

using namespace cyss;

void SpriteAnimation::addFrame(Rectangle& _frameRect)
{
    m_Frames.push_back(_frameRect);
}

Rectangle& SpriteAnimation::getFrame(int _n)
{
    Rectangle& ret = m_Frames[_n];
    return ret;
}

int SpriteAnimation::getFrameCount()
{
    return m_Frames.size();
}

SpriteAnimation::SpriteAnimation(std::string _name)
{
    m_Name = _name;
}

SpriteAnimation::~SpriteAnimation()
{
    m_Frames.clear();
}
