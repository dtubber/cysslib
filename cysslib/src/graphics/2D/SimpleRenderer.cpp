#include <cyss/graphics/2D/SimpleRenderer.hpp>

#include <GL/glew.h>

#define PI 3.14159

using namespace cyss;

SimpleRenderer::SimpleRenderer(unsigned int _width, unsigned int _height)
{
    strokeR = 255;
    strokeG = 255;
    strokeB = 255;
    fillR = 0;
    fillG = 0;
    fillB = 0;
    m_Width = _width;
    m_Height = _height;
    m_NoStroke = false;
    m_NoFill = false;
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

SimpleRenderer::~SimpleRenderer()
{
    
}

void SimpleRenderer::reset()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, m_Width, m_Height, 0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void SimpleRenderer::strokeColor(unsigned char _R, unsigned char _G, unsigned char _B)
{
    m_NoStroke = false;
    strokeR = _R;
    strokeG = _G;
    strokeB = _B;
}

void SimpleRenderer::noStroke()
{
    m_NoStroke = true;
}

void SimpleRenderer::fillColor(unsigned char _R, unsigned char _G, unsigned char _B)
{
    m_NoFill = false;
    fillR = _R;
    fillG = _G;
    fillB = _B;
}

void SimpleRenderer::noFill()
{
    m_NoFill = true;
}

void SimpleRenderer::drawLine(float _x, float _y, float _x2, float _y2)
{
    if (!m_NoStroke) 
    {
        glColor3ub(strokeR, strokeG, strokeB);
        glBegin(GL_LINES);
          glVertex2f((GLfloat)_x, (GLfloat)_y);
          glVertex2f((GLfloat)_x2, (GLfloat)_y2);
        glEnd();
    }
}

void SimpleRenderer::drawRectangle(float _x, float _y, float _width, float _height)
{
    if(!m_NoFill)
    {
        glColor3ub(fillR, fillG, fillB);
        glBegin(GL_QUADS);
          glVertex2f(_x, _y);
          glVertex2f(_x+_width, _y);
          glVertex2f(_x+_width, _y+_height);
          glVertex2f(_x, _y+_height);
        glEnd();
    }
    if(!m_NoStroke)
    {
        glColor3ub(strokeR, strokeG, strokeB);
        glBegin(GL_LINE_LOOP);
          glVertex2f((GLfloat)_x, (GLfloat)_y);
          glVertex2f((GLfloat)_x+_width, (GLfloat)_y);
          glVertex2f((GLfloat)_x+_width, (GLfloat)_y+_height);
          glVertex2f((GLfloat)_x, (GLfloat)_y+_height);
        glEnd();
    }
}

void SimpleRenderer::drawEllipse(float _x, float _y, float _width, float _height)
{
    
}

void SimpleRenderer::drawEllipse(float _x, float _y, float _width, float _height, unsigned int _segmentCount)
{
    if(!m_NoStroke)
    {
        glColor3ub(strokeR, strokeG, strokeB);
        glBegin(GL_LINE_LOOP);
        glEnd();
    }
}

void SimpleRenderer::drawArc(float _x, float _y, float _radius, float _angle)
{
    
}

void SimpleRenderer::drawArc(float _x, float _y, float _radius, float _angle, unsigned int _segmentCount)
{
    
}

void SimpleRenderer::drawArc(float _x, float _y, float _radius, float _from, float _to)
{
    
}

void SimpleRenderer::drawArc(float _x, float _y, float _radius, float _from, float _to, unsigned int _segmentCount)
{
    
}

void SimpleRenderer::drawTexture(Texture2D* _texture, float _x, float _y)
{
    unsigned int _width = _texture->getWidth();
    unsigned int _height = _texture->getHeight();
    
    _texture->setActive(0);
    glBegin(GL_QUADS);
      glTexCoord2f(0.0f, 0.0f);
      glVertex2f(_x, _y);
      
      glTexCoord2f(1.0f, 0.0f);
      glVertex2f(_x + _width, _y);
      
      glTexCoord2f(1.0f, 1.0f);
      glVertex2f(_x + _width, _y + _height);
      
      glTexCoord2f(0.0f, 1.0f);
      glVertex2f(_x, _y + _height);
    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    if(!m_NoStroke)
    {
        glColor3ub(strokeR, strokeG, strokeB);
        glBegin(GL_LINE_LOOP);
          glVertex2f((GLfloat)_x, (GLfloat)_y);
          glVertex2f((GLfloat)_x+_width, (GLfloat)_y);
          glVertex2f((GLfloat)_x+_width, (GLfloat)_y+_height);
          glVertex2f((GLfloat)_x, (GLfloat)_y+_height);
        glEnd();
    }
}

void SimpleRenderer::drawTexture(Texture2D* _texture, float _x, float _y, float _width, float _height)
{
    _texture->setActive(0);
    glBegin(GL_QUADS);
      glTexCoord2f(0.0f, 0.0f);
      glVertex2f(_x, _y);
      
      glTexCoord2f(1.0f, 0.0f);
      glVertex2f(_x + _width, _y);
      
      glTexCoord2f(1.0f, 1.0f);
      glVertex2f(_x + _width, _y + _height);
      
      glTexCoord2f(0.0f, 1.0f);
      glVertex2f(_x, _y + _height);
    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    if(!m_NoStroke)
    {
        glColor3ub(strokeR, strokeG, strokeB);
        glBegin(GL_LINE_LOOP);
          glVertex2f((GLfloat)_x, (GLfloat)_y);
          glVertex2f((GLfloat)_x+_width, (GLfloat)_y);
          glVertex2f((GLfloat)_x+_width, (GLfloat)_y+_height);
          glVertex2f((GLfloat)_x, (GLfloat)_y+_height);
        glEnd();
    }
}

void SimpleRenderer::pushMatrix()
{
    glPushMatrix();
}

void SimpleRenderer::popMatrix()
{
    glPopMatrix();
}

void SimpleRenderer::translateMatrix(float _x, float _y)
{
    glTranslatef( _x, _y, 0.0f);
}

void SimpleRenderer::rotateMatrix(float _radians, float _x, float _y)
{
    float degree = (_radians/(2*PI))*360;
    glRotatef(degree, _x, _y, 0);
}

void SimpleRenderer::lineThickness(float _thickness)
{
    glLineWidth(_thickness);
}

void SimpleRenderer::drawText(float _x, float _y, std::string _text)
{
    if(m_CurrentFont != NULL)
    {
        m_CurrentFont->setForegroundColor(strokeR, strokeG, strokeB);
        m_CurrentFont->draw(_x, _y, _text);
    }
}

void SimpleRenderer::setFont(MonochromeFont* _font)
{
    m_CurrentFont = _font;
}

void SimpleRenderer::setFontSize(float _size)
{
    if(m_CurrentFont != NULL)
    {
        m_CurrentFont->setSize(_size);
    }
}