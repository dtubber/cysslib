#include <cyss/graphics/VertexArray.hpp>

#include <string>
#include <GL/glew.h>

using namespace cyss;


VertexArray::VertexArray()
{
    glGenVertexArrays(1, &m_ID);
}

VertexArray::~VertexArray()
{
    glDeleteVertexArrays(1, &m_ID);
}

void VertexArray::draw(unsigned int _offset, unsigned int _count)
{
    glBindVertexArray(m_ID);
    glDrawArrays(GL_TRIANGLES, _offset, _count);
}

VertexBuffer* VertexArray::createNewBuffer(std::string _name, int _bytesize)
{
    VertexBuffer* ret = new VertexBuffer(_bytesize);
    m_Buffers[_name] = ret;
    return ret;
}

VertexBuffer* VertexArray::getBuffer(std::string _name)
{
    return m_Buffers[_name];
}

void VertexArray::deleteBuffer(std::string _name)
{
    VertexBuffer* buf = m_Buffers[_name];
    delete buf;
    m_Buffers.erase(_name);
}

unsigned int VertexArray::getID()
{
    return m_ID;
}
