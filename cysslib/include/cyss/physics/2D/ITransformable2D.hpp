#ifndef CYSSLIB_PHYSICS_2D_ITRANSFORMABLE2D_HPP
#define CYSSLIB_PHYSICS_2D_ITRANSFORMABLE2D_HPP

namespace cyss
{
    class ITransformable2D
    {
    public:
        virtual void setPosition(float _x, float _y) = 0;
        virtual void setRotation(float _degree) = 0;
        virtual void setScale(float _scale) = 0;
    };
}

#endif