#ifndef CYSSLIB_PHYSICS_VEC4F_HPP
#define CYSSLIB_PHYSICS_VEC4F_HPP

namespace cyss
{
    class vec4f 
    {
    public:
        vec4f();
        vec4f(float _x, float _y, float _z, float _w);
        ~vec4f();
        
        float getLength();
        vec4f operator+(const vec4f& _right);
        vec4f operator-(const vec4f& _right);
        vec4f operator*(const float _scalar);
        vec4f& operator=(const vec4f& _other);
        vec4f& operator+=(const vec4f& _other);
        vec4f& operator-=(const vec4f& _other);
        vec4f& operator*=(const float _scalar);
        
        static vec4f cross(const vec4f& _left, const vec4f& _right);
        static float dot(const vec4f& _left, const vec4f& _right);
        
        float X, Y, Z, W;
    };
}

#endif
