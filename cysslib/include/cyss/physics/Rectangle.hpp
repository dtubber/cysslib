#ifndef CYSSLIB_PHYSICS_RECTANGLE_HPP
#define CYSSLIB_PHYSICS_RECTANGLE_HPP

#include <glm/glm.hpp>

namespace cyss
{
    class Rectangle
    {
    public:
        Rectangle(float _x, float _y, float _width, float _height);
        ~Rectangle();
        
        float X, Y, Width, Height;
        
        bool intersects(Rectangle& _rect);
        bool intersects(glm::vec2& _vec);
    };
}

#endif