#ifndef CYSSLIB_GRAPHICS_2D_SPRITEANIMATION_HPP
#define CYSSLIB_GRAPHICS_2D_SPRITEANIMATION_HPP

#include <string>
#include <array>
#include <vector>

#include <cyss/physics/Rectangle.hpp>

namespace cyss
{
    //Forward declaration
    class Sprite;
    
    class SpriteAnimation
    {
    public:
        void addFrame(Rectangle& _frameRect);
        Rectangle& getFrame(int _n);
        int getFrameCount();
    private:
        friend class Sprite;
        SpriteAnimation(std::string _name);
        ~SpriteAnimation();
        
        std::vector<Rectangle> m_Frames;
        std::string m_Name;
    };
}

#endif