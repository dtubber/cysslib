#ifndef CYSSLIB_GRAPHICS_IDRAWABLE_HPP
#define CYSSLIB_GRAPHICS_IDRAWABLE_HPP

namespace cyss
{
    class IDrawable
    {
    public:
        virtual void draw() = 0;
    };
}

#endif