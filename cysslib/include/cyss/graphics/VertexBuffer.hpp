#ifndef CYSSLIB_GRAPHICS_VERTEXBUFFER_HPP
#define CYSSLIB_GRAPHICS_VERTEXBUFFER_HPP

#include <vector>

#include <glm/glm.hpp>

namespace cyss
{
    class VertexArray;
    
    enum VertexBufferType
    {
        
    };
    
    class VertexBuffer
    {
    public:
        unsigned int getID();
        
        void upload(std::vector<glm::vec4>& _vertices);
        void upload(std::vector<glm::mat4>& _matrices);
        void upload(std::vector<float>& _data);
        void upload(std::vector<char>& _data);
    private:
        friend class VertexArray;
        VertexBuffer(int _bytesize);
        ~VertexBuffer();
    
        unsigned int m_ID;
    };
}

#endif