#ifndef CYSSLIB_GRAPHICS_ANIM_MODEL_HPP
#define CYSSLIB_GRAPHICS_ANIM_MODEL_HPP

#include <memory>

#include <cyss/graphics/anim/Keyframe.hpp>

namespace cyss
{
    class Model
    {
    public:
        Model();
        ~Model();
        
        void draw();
        void update(std::shared_ptr<Keyframe> _keyframe);
    private:
        
    };
}

#endif