#ifndef CYSSLIB_GRAPHICS_ANIM_BONE_HPP
#define CYSSLIB_GRAPHICS_ANIM_BONE_HPP

#include <vector>
#include <hash>
#include <memory>
#include <string>

#include <glm/glm.hpp>

#include <cyss/graphics/anim/Keyframe.hpp>

namespace cyss
{
    class Bone
    {
    public:
        Bone(std::string _name);
        ~Bone();
        
        void update(std::shared_ptr<Keyframe> _keyframe);
        
        glm::mat4 getTransformation();
        glm::vec4 getBeginPosition();
        glm::vec4 getEndPosition();
    private:
        size_t m_ID;
        glm::vec4 m_Begin, m_End;
        glm::mat4 m_Transformation;
        std::vector<Bone*> m_Bones;
    };
}

#endif