#ifndef CYSSLIB_GRAPHICS_CAMERA_HPP
#define CYSSLIB_GRAPHICS_CAMERA_HPP

#include <glm/glm.hpp>

namespace cyss
{
    class Camera
    {
    public:
        Camera();
        ~Camera();
        
        glm::mat4 getProjMatrix();
        glm::mat4 getViewMatrix();
        
        void moveBy(glm::vec4 _delta);
        void setPosition(glm::vec4 _position);
        void lookAt(glm::vec4 _lookAt);
        void setUpVector(glm::vec4 _upVector);
        void projectOrtho();
        void projectPerspective();
        void rotate(float _degree, glm::vec4 _axes);
    private:
        glm::mat4 m_ProjMatrix;
        glm::mat4 m_ViewMatrix;
        
        glm::vec4 m_Position;
        glm::vec4 m_LookAt;
        glm::vec4 m_UpVector;
    };
}

#endif