#ifndef CYSSLIB_GRAPHICS_GAMEWINDOW_HPP
#define CYSSLIB_GRAPHICS_GAMEWINDOW_HPP

#include <string>
#include <mutex>

namespace cyss
{
    class InputManager;
    
    class GameWindow
    {
    public:
        GameWindow(std::string _name, unsigned int _width, unsigned int _height);
        ~GameWindow();
        
        void show();
        void hide();
        void resize(unsigned int _width, unsigned int _height);
        void clear();
        void swapBuffers();
        void setClearColor(unsigned char R, unsigned char G, unsigned char B);
        bool shouldClose();
        InputManager* getInputManager();
        void* getHandle();
        static void update();
    private:
        void* m_WindowHandle;
        InputManager* m_InputManager;
        static bool s_IsInitialized;
    };
}

#endif