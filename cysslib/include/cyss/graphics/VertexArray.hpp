#ifndef CYSSLIB_GRAPHICS_VERTEXARRAY_HPP
#define CYSSLIB_GRAPHICS_VERTEXARRAY_HPP

#include <unordered_map>
#include <memory>
#include <string>

#include <cyss/graphics/VertexBuffer.hpp>

namespace cyss
{
    class VertexArray
    {
    public:
        VertexArray();
        ~VertexArray();
        
        void draw(int _offset, int _count);
        
        VertexBuffer* createNewBuffer(std::string _name, int _bytesize);
        VertexBuffer* getBuffer(std::string _name);
        void deleteBuffer(std::string _name);
        
        void setAttribPointer(std::string _bufferName, int _index, VertexBufferType _dataType, int _size, int _stride, int _offset);
        void setAttribPointer(std::string _bufferName, int _index, VertexBufferType _dataType, int _size, int _stride, int _offset, bool _normalize);
        void enableAttribArray(std::string _bufferName, int _index);
        void disableAttribArray(std::string _bufferName, int _index);
        
        
        
        
        unsigned int getID();
    private:
        unsigned int m_ID;
        std::unordered_map<std::string, VertexBuffer*> m_Buffers;
    };
}

#endif