#ifndef CYSSLIB_AUDIO_SOUNDEFFECT_HPP
#define CYSSLIB_AUDIO_SOUNDEFFECT_HPP

#include <string>

namespace cyss
{
    class SoundEffect
    {
    public:
        SoundEffect(std::string _filename);
        ~SoundEffect();
        
        void play();
        void pause();
        void stop();
        void setShouldLoop(bool _shouldLoop);
        void setPitch(float _pitch);
        void setVolume(float _volume);
        void setOffsetS(float _offsetS);
        void setOffsetMS(int _offsetMS);
    private:
        void* m_BufferHandle;
        void* m_SoundHandle; 
    };
}

#endif