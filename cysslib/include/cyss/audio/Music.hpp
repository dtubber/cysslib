#ifndef CYSSLIB_AUDIO_MUSIC_HPP
#define CYSSLIB_AUDIO_MUSIC_HPP

#include <string>

namespace cyss
{
    class Music
    {
    public:
        Music(std::string _filename);
        ~Music();
        
        void play();
        void pause();
        void stop();
        void setPitch(float _pitch);
        void setShouldLoop(bool _shouldLoop);
        void setVolume(float _volume);
        void setOffsetS(float _offsetS);
        void setOffsetMS(int _offsetMS);
    private:
        void* m_MusicHandle;
    };
}

#endif