#ifndef CYSSLIB_UTIL_INPUTMAP_HPP
#define CYSSLIB_UTIL_INPUTMAP_HPP

#include <glm/glm.hpp>

#include <unordered_map>
#include <string>

#include <cyss/util/InputEnums.hpp>

namespace cyss
{
    class InputManager;
    
    class InputMap
    {
    public:
        void mapJoystickAxis(int _axisID, Joystick _joystick, JoystickAxis _deviceAxis);
        void mapJoystickButton(int _buttonID, Joystick _joystick, JoystickButton _deviceButton);
        void mapKeyboardButton(int _buttonID, KeyboardButton _deviceButton);
        void mapMouseButton(int _buttonID, MouseButton _deviceButton);
        
        bool getBoolWasDown(int _buttonID);
        float getFloatAxis(int _axisID);
        
        glm::vec2 getMousePosition();
        InputManager* getInputManager();
    private:   
        friend class InputManager;
        InputMap(InputManager* _mgr);
        ~InputMap();
        
        InputManager* m_Manager;
        
        std::unordered_map<int, bool> m_ButtonDownMap;
        std::unordered_map<int, float> m_AxisMap;
        
        std::unordered_map<int, int> m_KeyboardButtonMap;
        std::unordered_map<int, int> m_MouseButtonMap;
        std::unordered_map<int, std::unordered_map<int, int>> m_JoystickButtonMap;
        std::unordered_map<int, std::unordered_map<int, int>> m_JoystickAxisMap;
        
        glm::vec2 m_MousePosition;
    };
}

#endif