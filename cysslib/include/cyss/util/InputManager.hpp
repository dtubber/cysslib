#ifndef CYSSLIB_UTIL_INPUTMANAGER_HPP
#define CYSSLIB_UTIL_INPUTMANAGER_HPP

#include <string>
#include <unordered_map>

#include <cyss/graphics/GameWindow.hpp>
#include <cyss/util/InputEnums.hpp>
#include <cyss/util/InputMap.hpp>


namespace cyss
{
    class InputManager
    {
    public:
        void update();
        
        InputMap* createMap(std::string _name);
        InputMap* getMap(std::string _name);
        void destroyMap(std::string _name);
        
        bool getJoystickConnected(Joystick _joystick);
        std::string getJoystickName(Joystick _joystick);
        int getJoystickAxisNumber(Joystick _joystick);
        int getJoystickButtonNumber(Joystick _joystick);
        
        std::string getClipboardString();
        void setClipboardString(std::string _text);
    private:
        friend class GameWindow;
        InputManager(GameWindow* _window);
        ~InputManager();
        
        GameWindow* m_Window;
        std::unordered_map<std::string, InputMap*> m_InputMaps;
    };
}

#endif