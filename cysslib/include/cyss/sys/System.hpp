#ifndef CYSSLIB_SYS_SYSTEM_HPP
#define CYSSLIB_SYS_SYSTEM_HPP

#include <vector>
#include <string>
#include <cstddef>

#include <cyss/sys/Entity.hpp>

namespace cyss
{
    namespace ecs
    {
        class System
        {
        public:
            System();
            System(std::string _name);
            virtual ~System();
            
            void _update(std::vector<std::shared_ptr<Entity>>* _entities, float _deltaMS);
            void _update(std::vector<std::shared_ptr<Entity>>* _entities, float _deltaMS, bool _shouldUnload);
            
            std::string getName();
        private:
            void _load();
            void _unload();
        
            std::string m_Name;
            size_t m_UniqueID;
        protected:
            virtual void update(float _deltaMS) = 0;
            virtual void load() = 0;
            virtual void unload() = 0;
            
            bool m_IsLoaded;
            std::vector<std::shared_ptr<Entity>>* m_Entities;
        };
    }
}

#endif