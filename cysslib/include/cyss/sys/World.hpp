#ifndef CYSSLIB_SYS_WORLD_HPP
#define CYSSLIB_SYS_WORLD_HPP

#include <vector>
#include <unordered_map>
#include <string>
#include <memory>
#include <cstddef>
#include <mutex>

#include <cyss/sys/Entity.hpp>
#include <cyss/sys/System.hpp>

namespace cyss
{
    namespace ecs
    {
        class World
        {
        public:
            World();
            ~World();
            
            Entity* newEntity();
            Entity* newEntity(std::string _entityName);
            Entity* newEntityFromPrototype(std::string _prototypeName);
            Entity* newEntityFromPrototype(std::string _prototypeName, std::string _entityName);
            void deleteEntity(size_t _entityID);
            void addEntityPrototype(std::string _prototypeName, size_t _entityID);
            
            void addSystem(System* _system);
            void deleteSystem(size_t _systemID);
            
            void update(float _deltaMS);
        private:
            std::vector<std::shared_ptr<Entity>> m_Entities;
            std::mutex m_EntitiesMutex;
            std::vector<std::shared_ptr<System>> m_Systems;
            std::mutex m_SystemsMutex;
            std::unordered_map<std::string, std::shared_ptr<Entity>> m_EntityPrototypes;
            std::mutex m_EntityPrototypesMutex;
        };
    }
}

#endif