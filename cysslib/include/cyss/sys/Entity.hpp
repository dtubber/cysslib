#ifndef CYSSLIB_SYS_ENTITY_HPP
#define CYSSLIB_SYS_ENTITY_HPP

#include <unordered_map>
#include <string>
#include <memory>
#include <cstddef>
#include <mutex>

#include <cyss/sys/Component.hpp>

namespace cyss
{
    namespace ecs
    {
        class Entity
        {
        public:
            Entity();
            Entity(std::string _name);
            ~Entity();
            
            void addComponent(Component* _component);
            bool hasComponent(std::string _componentName);
            template<typename T>
            T* getComponent(std::string _componentName)
            {
                T* ret;
                ret = (T*)m_Components[_componentName].get();
                return ret;
            }
            std::shared_ptr<Component> removeComponent(std::string _componentName);
            void deleteComponent(std::string _componentName);
            
            std::string getName();
            size_t getID();
        private:
            std::unordered_map<std::string, std::shared_ptr<Component>> m_Components;
            std::string m_Name;
            size_t m_UniqueID;
            std::mutex m_Mutex;
        };
    }
}

#endif